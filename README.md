# Dockerize Angular 12

Angular 12 empty default project on an docker container

## Requisites

- Docker

## Create and Run Docker Container

```
docker build -t ng12-app -f dockerfile .
docker run --rm -t -p 4201:4200 ng12-app
```
